package com.example.kurpukproject

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mindorks.editdrawabletext.DrawablePosition
import com.mindorks.editdrawabletext.onDrawableClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            confirmLogin()
        }

        etPassword.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    target -> {
                        if (etPassword.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                            etPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance()
                        } else {
                            etPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance()
                        }
                    }
                }
            }
        })
    }

    private fun validateInput(): Boolean {
        if (etUsername.text.isEmpty() || etPassword.text.isEmpty()) {
            Toast.makeText(this, "Username Or Password can't be Empty!", Toast.LENGTH_SHORT).show()
            return false
        } else return true
    }

    fun confirmLogin() {
        if (validateInput()) {
            var username = etUsername.text.toString().trim()
            var pass = etPassword.text.toString().trim()
            var reported = "Login Failed"
            if (username.equals("Muvazana") && pass.equals("aldi")) {
                reported = "Login Successfuly"
            }

            Toast.makeText(this, reported, Toast.LENGTH_SHORT).show()
        }
    }
}
